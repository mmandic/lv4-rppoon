using System;
using System.Collections.Generic;
using System.Reflection;

namespace LV4_Zad3_Rppoon
{
    class Program
    {
        static void Main(string[] args)
        {
            RentingConsolePrinter rentingConsolePrinter = new RentingConsolePrinter();
            List<IRentable> printedItems = new List<IRentable>();
            printedItems.Add(new Book("Otac Goriot"));
            printedItems.Add(new Video("Moje djetinjstvo"));
            Console.WriteLine("Ukupna cijena je: ");
            rentingConsolePrinter.PrintTotalPrice(printedItems);
            Console.WriteLine(" U listi se nalaze:");
            rentingConsolePrinter.DisplayItems(printedItems);
        }
    }
}
