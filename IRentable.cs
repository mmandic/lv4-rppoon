using System;
using System.Collections.Generic;
using System.Text;

namespace LV4_Zad3_Rppoon
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();

    }
}
