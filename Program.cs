using System;
using System.IO;

namespace LV4_RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("File.txt");
            Analyzer3rdParty analyzer3RdParty = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer3RdParty);

            double[] averagePerColumn = adapter.CalculateAveragePerColumn(dataset);
            double[] averagePerRow = adapter.CalculateAveragePerRow(dataset);

            for(int i=0; i < averagePerColumn.Length; i++)
            {
                Console.WriteLine(averagePerColumn[i]);
            }
            for(int j=0; j< averagePerRow.Length; j++)
            {
                Console.WriteLine(averagePerRow[j]);
            }

        }
    }
}
