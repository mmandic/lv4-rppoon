using System;
using System.Collections.Generic;
using System.Text;

namespace LV4_RPPOON
{
    class Adapter : IAnalytics
    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }
        private double[][] ConvertData(Dataset dataset)
        {
            IList<List<double>> firstData = dataset.GetData();
            int rowCount = firstData.Count;
            int columnCount = firstData[0].Count;
            double[][] secondData = new double[rowCount][];
            for (int i = 0; i < rowCount; i++)
                secondData[i] = new double[columnCount];
            for (int i = 0; i < rowCount; i++)
                for (int j = 0; j < columnCount; j++)
                    secondData[i][j] = firstData[i][j];
            return secondData;
        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }

    }
}
